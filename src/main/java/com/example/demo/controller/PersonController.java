package com.example.demo.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.repository.PersonRepository;
import com.example.demo.model.Person;
 

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class PersonController {
	
	 @Autowired
	 private PersonRepository personRepository;
	 
	 @RequestMapping("/")
	 public List<Person> getAllEmployees() {
	        return personRepository.findAll();
	 }
	 
	 @GetMapping("/person/{id}")
	 public ResponseEntity<Person> getEmployeeById(@PathVariable(value = "id") Long personId)
	    throws ResourceNotFoundException {
	        Person person = personRepository.findById(personId)
	            .orElseThrow(() -> new ResourceNotFoundException("Person not found for this id :: " + personId));
	        return ResponseEntity.ok().body(person);
	 }
	 
	 @PostMapping("/person")
     public Person createPerson(@Valid @RequestBody Person person) {
        return personRepository.save(person);
     }
	 
	 @DeleteMapping("/person/{id}")
	 public Map<String, Boolean> deletePerson(@PathVariable(value = "id") Long personId)
		throws ResourceNotFoundException {
		 	Person person = personRepository.findById(personId)
            .orElseThrow(() -> new ResourceNotFoundException("Person not found for this id :: " + personId));

		personRepository.delete(person);
        Map<String, Boolean> response = new HashMap<> ();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
	 
	@PutMapping("/person/{id}")
    public ResponseEntity <Person> updateEmployee(@PathVariable(value = "id") Long personId,
        @Valid @RequestBody Person personDetails) throws ResourceNotFoundException {
        Person person = personRepository.findById(personId)
            .orElseThrow(() -> new ResourceNotFoundException("Person not found for this id :: " + personId));

        person.setName(personDetails.getName());
        person.setAddress(personDetails.getAddress()); 
        final Person updatedPerson = personRepository.save(person);
        return ResponseEntity.ok(updatedPerson);
    } 
}
